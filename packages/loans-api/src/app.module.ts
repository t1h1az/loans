import { Module } from '@nestjs/common';
import { LoanController } from './loan/loan.controller';
import { LoanService } from './loan/loan.service';
import { LoanModule } from './loan/loan.module';
import { AmortizationPlanService } from './loan/amortization.service';

@Module({
  imports: [LoanModule],
  controllers: [LoanController],
  providers: [LoanService, AmortizationPlanService],
})
export class AppModule {}
