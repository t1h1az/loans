import { Module } from '@nestjs/common';
import { LoanService } from './loan.service';
import { LoanController } from './loan.controller';
import { AmortizationPlanService } from './amortization.service';

@Module({
  imports: [],
  controllers: [LoanController],
  providers: [LoanService, AmortizationPlanService],
})
export class LoanModule {}
