import { IsNumber, IsPositive } from 'class-validator';
import { AmortizationPlanDTO } from './AmortizationPlan.dto';

export class PaymentDetails {
  @IsNumber()
  @IsPositive()
  readonly monthlyRate: number;

  @IsNumber()
  @IsPositive()
  readonly remainingDebt: number;

  readonly amortizationPlan: AmortizationPlanDTO[];
}
