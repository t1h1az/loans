export class CreateCalculationDTO {
  readonly interestRate: string;
  readonly loanAmount: string;
  readonly fixedInterestPeriod: string;
  readonly downPayment: string;
}
