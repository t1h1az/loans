import { IsNumber, IsPositive } from 'class-validator';

export class AmortizationPlanDTO {
  @IsNumber()
  @IsPositive()
  readonly year: number;

  @IsNumber()
  @IsPositive()
  readonly annuity: number;

  @IsNumber()
  readonly remainingDebt: number;

  @IsNumber()
  @IsPositive()
  readonly yearlyDebtReduction: number;

  @IsNumber()
  @IsPositive()
  readonly yearlyInterestFee: number;
}
