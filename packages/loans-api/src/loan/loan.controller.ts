import { Controller, Get, Query } from '@nestjs/common';
import { CreateCalculationDTO } from './dto/LoanCaculation.dto';
import { PaymentDetails } from './dto/PaymentDetail.dto';
import { LoanService } from './loan.service';

@Controller('loan')
export class LoanController {
  constructor(private readonly loanService: LoanService) {}

  @Get('calculate')
  calculate(
    @Query()
    createCalculationDTO: CreateCalculationDTO,
  ): PaymentDetails {
    return this.loanService.calculatePaymentDetails(createCalculationDTO);
  }
}
