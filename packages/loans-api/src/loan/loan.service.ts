import { Injectable } from '@nestjs/common';
import { AmortizationPlanService } from './amortization.service';
import { AmortizationPlanDTO } from './dto/AmortizationPlan.dto';
import { CreateCalculationDTO } from './dto/LoanCaculation.dto';
import { PaymentDetails } from './dto/PaymentDetail.dto';

@Injectable()
export class LoanService {
  constructor(
    private readonly amortizationPlanService: AmortizationPlanService,
  ) {}

  private calculateAnnualRate(createCalculationDTO: CreateCalculationDTO) {
    const { interestRate, loanAmount, fixedInterestPeriod, downPayment } =
      createCalculationDTO;
    const k0 = parseInt(loanAmount);
    const n = parseInt(fixedInterestPeriod);
    const q = 1 + parseInt(interestRate) / 100;
    return (k0 * Math.pow(q, n) * (q - 1)) / (Math.pow(q, n) - 1);
  }

  calculatePaymentDetails(
    createCalculationDTO: CreateCalculationDTO,
  ): PaymentDetails {
    const annualRate = this.calculateAnnualRate(createCalculationDTO);
    const amortizationPlan: AmortizationPlanDTO[] =
      this.amortizationPlanService.createAmortizationPlan(
        createCalculationDTO,
        annualRate,
      );

    return {
      remainingDebt:
        amortizationPlan[amortizationPlan.length - 1]['remainingDebt'],
      monthlyRate: annualRate / 12,
      amortizationPlan,
    };
  }
}
