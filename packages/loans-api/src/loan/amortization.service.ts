import { Injectable } from '@nestjs/common';
import { AmortizationPlanDTO } from './dto/AmortizationPlan.dto';
import { CreateCalculationDTO } from './dto/LoanCaculation.dto';

@Injectable()
export class AmortizationPlanService {
  private annualRate: number;
  private remainingDebt: number;
  private interestRate: number;

  private calculateAmortization(
    remainingDebt: number,
    index: number,
  ): AmortizationPlanDTO {
    const yearlyInterestFee = remainingDebt * this.interestRate;
    const yearlyDebtReduction = this.annualRate - yearlyInterestFee;
    this.remainingDebt = this.remainingDebt - yearlyDebtReduction;

    return {
      year: index,
      annuity: this.annualRate,
      remainingDebt: this.remainingDebt,
      yearlyDebtReduction,
      yearlyInterestFee,
    };
  }

  public createAmortizationPlan(
    createCalculationDTO: CreateCalculationDTO,
    annualRate: number,
  ): AmortizationPlanDTO[] {
    const amortizationPlan: AmortizationPlanDTO[] = [];
    this.annualRate = annualRate;
    this.remainingDebt = parseInt(createCalculationDTO.loanAmount);
    this.interestRate = parseInt(createCalculationDTO.interestRate) / 100;

    for (
      let index = 1;
      index <= parseInt(createCalculationDTO.fixedInterestPeriod);
      index++
    ) {
      const amortizations: AmortizationPlanDTO = this.calculateAmortization(
        this.remainingDebt,
        index,
      );
      amortizationPlan.push(amortizations);
    }
    return amortizationPlan;
  }
}
