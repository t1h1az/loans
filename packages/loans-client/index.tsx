import React from "react";
import ReactDOM from "react-dom";
import App from "./src/main";

const isProd = process.env.NODE_ENV === 'production';

if (isProd) {
  console.log('Logger follows...')
}

ReactDOM.render(<App/>, document.getElementById("loan_calculator"));
