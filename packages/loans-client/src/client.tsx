export async function get(path: string) {
  const headers = new Headers();
  headers.set('Content-Type', 'application/json');

  let response;

  try {
    response = await fetch(path, {
      method: 'GET',
      headers: headers,
    });
  } catch (e) {
    throw new Error(`Api call Fail with method="GET", url="${path}"`);
  }
  return await response.json();
}
