declare module '*.svg' {
  const content: any;
  export default content;
}

declare module '*.png' {
  const path: string;
  export default path;
}

interface LoanFormData {
  interestRate: number;
  downPayment: Date;
  duration: string;
  loan: string;
}

interface Amortization {
  year: number;
  annuity: number;
  remainingDebt: number;
  yearlyDebtReduction: number;
  yearlyInterestFee: number;
}
interface Loan {
  monthlyRate: number;
  remainingDebt: number;
  amortizationPlan: Amortization[];
}

interface IconProp {
  name: string;
  color: string;
}
