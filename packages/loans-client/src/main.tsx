import React from 'react';
import LoanCalculatorView from './components/LoanCalculatorView/LoanCalculatorView';
import Header from './components/common/Header/Header';
import logo from './assets/images/logo.png';
import './main.css';

// Todo: add themeing
const App = () => {
  return (
    <>
      <Header brandLogo={logo}/>
      <LoanCalculatorView />
    </>
  );
};

export default App;
