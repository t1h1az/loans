import Translations from '../assets/translations/de.json';

export default (translationId: string, defaultTranslation?: string, domain?: string) => {
  let translation: any = Translations;

  if (domain) {
    translation = translation[domain];
  }
  
  for (let key in translation) {
    if (key === translationId) {
      return translation[key];
    }
  }

  return defaultTranslation ? defaultTranslation : translationId;
}
