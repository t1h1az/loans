export default (
  value: number,
  languageISO = 'de-DE',
  currency = 'EUR',
  style = 'currency'
): string => {
  return new Intl.NumberFormat(languageISO, {
    style,
    currency,
    signDisplay: 'never',
  }).format(value);
};
