import React from 'react';
import './skeleton-table.css';

const Table = () => {
  return (
    <div className="skeleton-table__container">
      <div className="skeleton-table__navigation">
        <div className="skeleton skeleton-table__navigation--left"></div>
        <div className="skeleton skeleton-table__navigation--right"></div>
      </div>
      <div className="skeleton-table__header">
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
      </div>
      <div className="skeleton-table__header">
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
      </div>
      <div className="skeleton-table__header">
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
      </div>
      <div className="skeleton-table__header">
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
        <div className="skeleton skeleton-table__head"></div>
      </div>
    </div>
  );
}

export default Table;
