import React from 'react';

interface HeaderProps {
  brandLogo: any
}

export default function Header({brandLogo}: HeaderProps) {
  return (
    <header>
       <img src={brandLogo} width={'200px'}/>
    </header>
  );
}
