import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function createData(
  year: number,
  yearlyDebtReduction: number,
  yearlyInterestFee: number,
  annuity: number,
  remainingDebt: number,
) {
  return { year, yearlyDebtReduction, yearlyInterestFee, annuity, remainingDebt };
}

interface Props {
    data: Amortization[];
    transform: Function;
}

export default function BasicTable({ data, transform}: Props) {
  return (
    <TableContainer>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Year</TableCell>
            <TableCell align="right">Payments</TableCell>
            <TableCell align="right">Interest Rate</TableCell>
            <TableCell align="right">Annuity</TableCell>
            <TableCell align="right">Remaining Debt</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row: Amortization) => (
            <TableRow
              key={row.year}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.year}
              </TableCell>
              <TableCell align="right">{transform ? transform(row.yearlyDebtReduction) : row.yearlyDebtReduction}</TableCell>
              <TableCell align="right">{transform ? transform(row.yearlyInterestFee) : row.yearlyInterestFee}</TableCell>
              <TableCell align="right">{transform ? transform(row.annuity) : row.annuity}</TableCell>
              <TableCell align="right">{transform ? transform(row.remainingDebt) : row.remainingDebt}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}