import React from 'react';
import {Box, Typography} from '@mui/material';
import BasicTable from '../common/Table/BasicTable';
import SkeletonTable from '../common/SkeletonLoaders/SkeletonTable';
import formatCurrency from '../../helpers/formatCurrency';
import './amortization-plan.css';

interface Props {
  amortizationPlan: Amortization[];
  loading: Boolean;
}

const AmortizationPlan = ({amortizationPlan, loading}: Props) => {
  return (
    <Box
    sx={{ width: '100%' }}
    >
      {loading && (
        <section className="loan-calculator row">
          <SkeletonTable />
        </section>
      )}
      {!loading && (
        <>
          <div className="heading">
              Amortization Plan
          </div>
          <div className="box">
            <BasicTable data={amortizationPlan} transform={formatCurrency} />
          </div>
        </>
      )}
    </Box>
  );
};

export default AmortizationPlan;
