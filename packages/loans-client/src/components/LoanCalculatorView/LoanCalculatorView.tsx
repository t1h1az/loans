import React, {useState} from 'react';
import './loan-calculator.css';
import '../../main.css';
import LoanCalculatorForm from '../LoanCalculatorForm/LoanCalculatorForm';
import AmortizationPlan from '../AmortizationPlan/AmortizationPlan';
import PaymentDetails from '../PaymentDetails/PaymentDetails';
import {Box, Grid} from '@mui/material';

const LoanCalculatorView = () => {
  const [monthlyRate, setMonthlyRate] = useState<number>(0);
  const [remainingDebt, setRemainingDebt] = useState<number>(0);
  const [amortizationPlan, setAmortizationPlan] = useState<Amortization[]>();
  const [loading, setLoading] = useState<Boolean>(false);

  return (
    <section>
      <Box sx={{flexGrow: 1}}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <LoanCalculatorForm
              setMonthlyRate={setMonthlyRate}
              setRemainingDebt={setRemainingDebt}
              setAmortizationPlan={setAmortizationPlan}
              setLoading={setLoading}
            />
          </Grid>
          {monthlyRate && remainingDebt ? (
            <Grid item xs={12} md={6}>
              <PaymentDetails
                monthlyRate={monthlyRate}
                remainingDebt={remainingDebt}
              />
            </Grid>
          ) : null}
          {amortizationPlan ? (
            <Grid item xs={12}>
              <AmortizationPlan
                loading={loading}
                amortizationPlan={amortizationPlan}
              />
            </Grid>
          ) : null}
        </Grid>
      </Box>
    </section>
  );
};

export default LoanCalculatorView;
