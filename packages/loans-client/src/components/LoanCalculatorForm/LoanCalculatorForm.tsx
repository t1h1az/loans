import React, {useEffect, useState} from 'react';
import Button from '@mui/material/Button';
import {Box, Switch, TextField} from '@mui/material';
import {calculateLoanPayment} from '../../api/loan';
import './loan-calculator-form.css';

interface Props {
  setMonthlyRate: Function;
  setRemainingDebt: Function;
  setAmortizationPlan: Function;
  setLoading: Function;
}

// Todo: add input validators, error labels and styling
// Todo: prevent downpayment number being as big or bigger than loan amount, discuss excluding absurd numbers

const LoanCalculatorForm = ({
  setMonthlyRate,
  setRemainingDebt,
  setAmortizationPlan,
  setLoading,
}: Props) => {
  const [interestRate, setInterestRate] = useState<string>('5');
  const [loanAmount, setLoanAmount] = useState<string>('120000');
  const [downPayment, setDownPayment] = useState<string>('20000');
  const [fixedInterestRates, setFixedInterestRates] = useState<string>('30');
  const [status, setStatus] = useState<string>('idle');
  const [preloaded, setPreloaded] = useState<boolean>(false);
  const [useFixedInterestRates, setUseFixedInterestRates] =
    useState<Boolean>(false);

  useEffect(() => {
    if (preloaded && status === 'submit') {
      setLoading(true);
      const fetch = async () => {
        const paymentDetails = await calculateLoanPayment(
          parseInt(interestRate),
          parseInt(loanAmount),
          parseInt(downPayment),
          parseInt(fixedInterestRates)
        );
        const {monthlyRate, remainingDebt, amortizationPlan} =
          await paymentDetails;
        setMonthlyRate(monthlyRate);
        setRemainingDebt(remainingDebt);
        setAmortizationPlan(amortizationPlan);
        setLoading(false);
        setStatus('idle');
      };
      fetch();
    }
  }, [status]);

  return (
    <section className="loan-calculator form">
      <div className="heading">Calculate loan rates</div>
      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': {m: 1, width: '95%'},
        }}
        noValidate
        autoComplete="off"
        className="box"
      >
        <TextField
          required
          id="standard-basic"
          label="Loan amount"
          variant="standard"
          value={loanAmount}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setLoanAmount(event.target.value);
          }}
          onBlur={() => {
            if (preloaded) {
              setStatus('submit');
            }
          }}
        />
        <TextField
          required
          type={'number'}
          id="standard-basic"
          label="Interest Rate"
          variant="standard"
          value={interestRate}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setInterestRate(event.target.value);
          }}
          onBlur={() => {
            if (preloaded) {
              setStatus('submit');
            }
          }}
        />
        <TextField
          required
          id="standard-basic"
          label="Down payment"
          variant="standard"
          value={downPayment}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setDownPayment(event.target.value);
          }}
          onBlur={() => {
            if (preloaded) {
              setStatus('submit');
            }
          }}
        />
        <div className="fixed-period__container column">
          <div className="row">
            <p>Do you want to change interest period?</p>
            <Switch
              onChange={() => setUseFixedInterestRates(!useFixedInterestRates)}
            />
          </div>
          {useFixedInterestRates ? (
            <TextField
              type={'number'}
              id="standard-basic"
              label="Fixed Interest Period"
              variant="standard"
              value={fixedInterestRates}
              fullWidth
              InputProps={{inputProps: {min: 1, max: 99}}}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setFixedInterestRates(event.target.value);
              }}
              onBlur={() => {
                if (preloaded) {
                  setStatus('submit');
                }
              }}
            />
          ) : null}
        </div>
        <Button
          variant="outlined"
          style={{color: 'red', border: '1px solid red'}}
          onClick={() => {
            setPreloaded(true);
            setStatus('submit');
          }}
          className="margin-8 float-right"
        >
          Let's go
        </Button>
      </Box>
    </section>
  );
};

export default LoanCalculatorForm;
