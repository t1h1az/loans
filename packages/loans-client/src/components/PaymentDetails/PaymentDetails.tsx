import {Typography} from '@mui/material';
import {Box} from '@mui/system';
import React from 'react';
import formatCurrency from '../../helpers/formatCurrency';
import './payment-details.css';

interface Props {
  monthlyRate: number;
  remainingDebt: number;
}

const PaymentDetails = ({monthlyRate, remainingDebt}: Props) => {
  return (
    <>
      <div className="heading">
          Loan Rate
      </div>
      <Box className="box" sx={{padding: '1rem'}}>
        <div className="row">
          <Typography variant="overline" component="span">
            Monthly Rate
          </Typography>
          <Typography variant="h6" component="span">
            {formatCurrency(monthlyRate)}
          </Typography>
        </div>
        { (remainingDebt > 0) ??
        <div className="row">
          <Typography variant="overline" component="span">
            Remaining Debt
          </Typography>
          <Typography variant="h6" component="span">
            {formatCurrency(remainingDebt)}
          </Typography>
        </div>
        }
      </Box>
    </>
  );
};

export default PaymentDetails;
