import {get} from '../client';
const localhost = 'http://localhost:3000';

export const calculateLoanPayment = async (
  interestRate: number = 5,
  loanAmount: number = 120000,
  downPayment: number = 20000,
  fixedInterestPeriod: number = 30
): Promise<Loan> => {
  const data = await get(
    `${localhost}/loan/calculate?interestRate=${interestRate}&loanAmount=${loanAmount}&downPayment=${downPayment}&fixedInterestPeriod=${fixedInterestPeriod}`
  );
  return data;
};
