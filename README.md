# S-Communications 
## Loan Prototype

### Clone repo
    $ git clone repo

### Local development

### Run npm install in client and api
    $ cd packages/loans-client
    $ npm ci
    $ cd packages/loans-api
    $ npm ci

### Serve api
    $ cd packages/loans-api
    $ npm run start


### Serve client
    $ cd packages/loans-client
    $ npm run dev

